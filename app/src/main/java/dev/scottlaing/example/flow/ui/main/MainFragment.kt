package dev.scottlaing.example.flow.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import dev.scottlaing.example.domain.SDK
import dev.scottlaing.example.domain.StateChange
import dev.scottlaing.example.flow.R
import kotlinx.android.synthetic.main.main_fragment.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val stateChange = StateChange { state ->
        start_button.isEnabled = state.canStartPolling
        stop_button.isEnabled = !state.canStartPolling

        message.text =
            state.teams.joinTo(StringBuilder(), "\n") { team -> "${team.location} ${team.name}" }
        timestamp.text = state.timestamp
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        start_button.setOnClickListener {
            SDK.instance.start()
        }
        stop_button.setOnClickListener {
            SDK.instance.stop()
        }
        SDK.instance.initialise(stateChange)
    }

}