package dev.scottlaing.example.domain

import dev.scottlaing.example.data.IRepository
import dev.scottlaing.example.data.Team
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch

data class State(val timestamp: String, val teams: List<Team>, val canStartPolling: Boolean)

class TeamsUsecase(private val repository: IRepository) {

    private val job = Job()
    private val scope = CoroutineScope(Dispatchers.Default + job)

    private var stateChange: StateChange? = null

    fun initialise(stateChange: StateChange) {
        this.stateChange = stateChange
    }

    fun start() {
        updateState(State("Polling started", emptyList(), false))
        scope.launch {
            repository.allTeams.collect { response ->
                updateState(State(response.timestamp.toString(), response.teams, false))
            }
        }
    }

    fun stop() {
        updateState(State("Polling ended", emptyList(), true))
        repository.stop()
    }

    private fun updateState(state: State) {
        CoroutineScope(Dispatchers.Main).launch {
            stateChange?.onStateChange(state)
        }
    }
}
