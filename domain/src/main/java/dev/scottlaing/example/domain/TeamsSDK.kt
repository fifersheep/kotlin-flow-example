package dev.scottlaing.example.domain

import dev.scottlaing.example.data.IRepository
import dev.scottlaing.example.data.IRestAPI
import dev.scottlaing.example.data.Repository
import dev.scottlaing.example.data.RestAPI

object SDK {
    val instance: TeamsSDK by lazy { TeamsSDK() }
}

class TeamsSDK internal constructor() {
    private val restAPI: IRestAPI by lazy {
        RestAPI()
    }

    private val repository: IRepository by lazy {
        Repository(restAPI)
    }

    private val usecase: TeamsUsecase by lazy {
        TeamsUsecase(repository)
    }

    fun initialise(stateChange: StateChange) = usecase.initialise(stateChange)

    fun start() {
        usecase.start()
    }

    fun stop() {
        usecase.stop()
    }
}