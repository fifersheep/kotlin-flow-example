package dev.scottlaing.example.domain

fun interface StateChange {
    fun onStateChange(state: State)
}