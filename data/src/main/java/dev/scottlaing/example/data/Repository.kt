package dev.scottlaing.example.data

import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.*

data class Response(val timestamp: Date, val teams: List<Team>)

interface IRepository {
    val allTeams: Flow<Response>
    fun stop()
}

class Repository(private val restAPI: IRestAPI) : IRepository {
    var shouldPoll = false

    override val allTeams: Flow<Response> = flow {
        shouldPoll = true
        while (shouldPoll) {
            val response = restAPI.allTeams()
            emit(Response(Date(), response))
            delay(10_000)
        }
    }

    override fun stop() {
        shouldPoll = false
    }
}
