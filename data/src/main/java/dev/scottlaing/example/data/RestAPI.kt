package dev.scottlaing.example.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

interface IRestAPI {
    suspend fun allTeams(): List<Team>
}

class RestAPI : IRestAPI {
    private val data = listOf(
        Team("Edinburgh", "Wolves"),
        Team("Manchester", "Titans"),
        Team("Merseyside", "Nighthawks"),
        Team("Sandwell", "Steelers"),
        Team("Sheffield", "Giants"),
        Team("Tamworth", "Phoenix")
    )

    override suspend fun allTeams(): List<Team> = withContext(Dispatchers.IO) {
        delay(3000)
        data
    }
}
