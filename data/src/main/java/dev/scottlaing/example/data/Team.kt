package dev.scottlaing.example.data

data class Team(
    val location: String,
    val name: String
)